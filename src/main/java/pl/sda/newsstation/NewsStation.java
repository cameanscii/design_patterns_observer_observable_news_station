package pl.sda.newsstation;

import java.util.Observable;

public class NewsStation extends Observable {

    public void inform(Object message){
        setChanged();
        notifyObservers(message);
    }
}
