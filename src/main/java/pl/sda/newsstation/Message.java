package pl.sda.newsstation;

public class Message {
    private String content;
    private int messageImportanceLevel; // 0 to 10

    public Message(String content, int messageImportanceLevel) {
        this.content = content;
        this.messageImportanceLevel = messageImportanceLevel;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getMessageImportanceLevel() {
        return messageImportanceLevel;
    }

    public void setMessageImportanceLevel(int messageImportanceLevel) {
        this.messageImportanceLevel = messageImportanceLevel;
    }
}
