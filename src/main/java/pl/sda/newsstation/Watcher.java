package pl.sda.newsstation;

import java.util.Observable;
import java.util.Observer;

public class Watcher implements Observer {

    private String name;
    int panicLevel; // depend of importance of message

    public Watcher(String name, int panicLevel) {
        this.name = name;
        this.panicLevel = panicLevel;

    }

    public void update(Observable o, Object arg) {
        if (o instanceof NewsStation && arg instanceof Message){
            if(((Message) arg).getMessageImportanceLevel()>panicLevel){
                System.out.println("Panic level is exceeded for user: "+name);
                return;
            }
        }
        System.out.println(name + " informed about: "+arg);
    }
}
